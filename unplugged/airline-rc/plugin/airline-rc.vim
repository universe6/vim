let g:airline_powerline_fonts = 1
set laststatus=2
let g:airline#extensions#tabline#enabled = 1

"set shortmess=atI " Abbreviate messages
