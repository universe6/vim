
let g:ctrlp_types = ['mru', 'buf', 'fil']

let g:ctrlp_cache_dir = expand('~/.vim.local/ctrlp')

if executable('rg')
  let g:ctrlp_user_command = 'rg %s --files --color=never --glob ""'
endif

