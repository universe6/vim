autocmd User Startified setlocal cursorline

noremap <leader>s :Startify<Cr> 

let g:startify_disable_at_vimenter = 1

let g:startify_enable_special         = 0
let g:startify_files_number           = 10
let g:startify_relative_path          = 1
let g:startify_change_to_dir          = 1
let g:startify_session_autoload       = 1
let g:startify_session_persistence    = 1
let g:startify_session_delete_buffers = 1
let g:startify_custom_header = []
let g:startify_session_dir = '~/.vim.local/session'

let g:startify_list_order =
    \ [
        \ ['Most Recently Used'], 'files',
        \ [expand('%:p:h')], 'dir',
        \ ['Sessions:'], 'sessions',
        \ ['Bookmarks:'], 'bookmarks',
    \ ]

" let g:startify_skiplist = [
"             \ 'COMMIT_EDITMSG',
"             \ 'bundle/.*/doc',
"             \ '/data/repo/neovim/runtime/doc',
"             \ '/Users/mhi/local/vim/share/vim/vim74/doc',
"             \ ]
"
" let g:startify_bookmarks = [
"             \ { 'v': '~/.vimrc' },
"             \ { 't': '/tmp' },
"             \ ]

if has("gui_running")
    hi StartifyBracket guifg=#585858
    " hi StartifyFile    guifg=#afafff
    " hi StartifyFooter  guifg=#585858
    " hi StartifyHeader  guifg=#87df87
    " hi StartifyNumber  guifg=#ffaf5f
    hi StartifyPath    guifg=#8a8a8a
    hi StartifySlash   guifg=#585858
    hi StartifySpecial guifg=#585858
else
    hi StartifyBracket ctermfg=240
    " hi StartifyFile    ctermfg=147
    " hi StartifyFooter  ctermfg=240
    " hi StartifyHeader  ctermfg=114
    " hi StartifyNumber  ctermfg=215
    hi StartifyPath    ctermfg=245
    hi StartifySlash   ctermfg=240
    hi StartifySpecial ctermfg=240
endif
