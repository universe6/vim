let g:syntastic_xml_xmllint_args = "--xinclude --postvalid --nonet"

let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0
let g:syntastic_mode="passive"
