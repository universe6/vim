
nmap gx <Plug>(openbrowser-smart-search)
vmap gx <Plug>(openbrowser-smart-search)

let g:openbrowser_default_search = 'DuckDuckGo'
let g:openbrowser_search_engines = {
\   'DuckDuckGo': 'https://duckduckgo.com/?q={query}'
\}

