
let NERDTreeMouseMode=3

if exists('g:loaded_nerdtree_rc')
  finish
endif

let NERDTreeMinimalUI=1

nnoremap <TAB> :NERDTreeToggle \| call nerdtree#ui_glue#invokeKeyMap("CD") <CR>

let g:nerdtree_tabs_open_on_gui_startup=0
let g:nerdtree_tabs_open_on_console_startup=0

let g:loaded_nerdtree_rc = 1

function s:sid_ui_glue()
    if !exists("s:sid")
        redir => g:scriptnames
        silent scriptnames
        redir END
        let s:sid = matchstr(g:scriptnames,'\d*\ze:[^\n]*ui_glue')
    endif
    return '<SNR>' . s:sid . '_'
endfun

let s = s:sid_ui_glue()

call NERDTreeAddKeyMap({ 'key': '<Right>', 'scope': "FileNode", 'callback': s."previewNodeCurrent" })
call NERDTreeAddKeyMap({ 'key': '<Right>', 'scope': "DirNode", 'callback': s."activateDirNode" })
call NERDTreeAddKeyMap({ 'key': '<Left>', 'scope': "Node", 'callback': s."closeCurrentDir" })

" ------ multi-cursor and neo-complete compatibility fix

" Called once right before you start selecting multiple cursors
function! Multiple_cursors_before()
    if exists(':NeoCompleteLock')==2
        exe 'NeoCompleteLock'
    endif
endfunction

" Called once only when the multiple selection is canceled (default <Esc>)
function! Multiple_cursors_after()
    if exists(':NeoCompleteUnlock')==2
        exe 'NeoCompleteUnlock'
    endif
endfunction


