set guioptions-=e
set guioptions+=r
set guioptions-=L
set guioptions-=T

if has('win32') || has('win64')
    set guifont=MesloLGSDZ_NF:h10:cANSI:qDRAFT
    set columns=140
    set lines=40
endif

if (has("win32unix"))
endif

if (has("mac"))
    set guifont=MesloLGSDZNerdFontCompleteM-Regular:h11
endif

if (has("unix") && !has("mac") && !has("win32unix"))
    set guifont=MesloLGSDZ\ Nerd\ Font\ Mono\ 10
    set columns=140
    set lines=40
endif

if filereadable(expand("~/.vim.local/gvimrc"))
  source ~/.vim.local/gvimrc
endif


" vim:ft=vim
