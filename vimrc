if has('win32') || has('win64')
    set runtimepath=~/.vim,$VIMRUNTIME,~/.vim/after
endif

let loaded_netrwPlugin = 1

set backup
set backupdir^=~/.vim.local/backup//

if has('unix') || has('mac')
  set backupskip&
  set backupskip^=/dev/shm/*,/usr/tmp/*,/var/tmp/*
endif

set swapfile
set directory=~/.vim.local/swap//

" set completeopt+=longest

syntax on
filetype plugin on
filetype indent on

let mapleader=","
let maplocalleader=","

if filereadable(expand("~/.vim/plugrc"))
  source ~/.vim/plugrc
endif

nnoremap  Y y$

noremap  ; :
noremap  : ;
noremap  q; q:
 
noremap  <c-f> f
noremap  <c-b> t
noremap  f <c-f>
noremap  t <c-b>

" execute current line
nnoremap <silent> <leader>vc yy:<C-f>p<C-c>

" collapse duplicate lines
noremap <leader>md :%s/^\(.*\)\(\n\1\)\+$/\1/<cr>
" collapse duplicate blank lines
noremap <leader>mb :%s/^\([\s]*\)\n\1\+$/\1<cr>


set number
set showcmd
set showmode
set visualbell

set hidden

set nrformats-=octal
set fileformats+=mac

set autoindent
set smartindent
set smarttab
set expandtab
set shiftwidth=4 tabstop=4 softtabstop=4
set complete-=i

set backspace=indent,eol,start
set history=1000

set nowrap
set linebreak

set foldmethod=indent
set foldnestmax=3
set nofoldenable

set wildmode=list:full
set wildmenu

set ignorecase
" set smartcase

set listchars="tab:›-,trail:·,nbsp:•"
set scrolloff=0 sidescrolloff=0
set tabpagemax=40
set nostartofline
set clipboard=unnamed
set report=0
set whichwrap=<,>,[,],h,l " allow cursorwrapping for arrowkeys
set mps+=<:> " allow also < and > for the % command

let delimitMate_matchpairs = "(:),[:],{:}"

set sessionoptions-=options

set mouse=a
set gcr=a:blinkon0

set encoding=utf-8

autocmd VimResized * wincmd =

set autoread
let v:fcs_choice="ask"
if !has("gui_running")
    " au FocusGained,BufEnter,InsertEnter * :silent! !
    au FocusGained,BufEnter,InsertEnter * :checktime
endif

if !has('nvim')
  set viminfo='50,<100,s100,:1000,h,n$HOME/.vim.local/viminfo
endif



if filereadable(expand("~/.vim.local/vimrc"))
  source ~/.vim.local/vimrc
endif

" vim:ft=vim
